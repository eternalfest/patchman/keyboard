# 0.10.0 (2021-04-21)

- **[Breaking change]** Update to `patchman@0.10.3`.
- **[Internal]** Update to Yarn 2.

# 0.9.0 (2020-09-02)

- **[Breaking change]** Update to `patchman@0.9.0`.

# 0.8.0 (2020-01-08)

- **[Breaking change]** Update to `patchman@0.8.0`.

# 0.7.1 (2020-01-07)

- **[Fix]** Update dependencies.

# 0.7.0 (2019-12-10)

- **[Breaking change]** Update to `patchman@0.7.1`.

# 0.2.0 (2019-11-02)

- **[Breaking change]** Update to `patchman@0.6.2`.
- **[Internal]** Use `yarn` to generate lock file.

# 0.1.0 (2019-09-19)

- **[Feature]** First release.
