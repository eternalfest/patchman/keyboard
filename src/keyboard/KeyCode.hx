package keyboard;

/**
 * An opaque keyboard code.
 */
abstract KeyCode(Int) {
  public static inline var ALT: KeyCode = new KeyCode(18);
  public static inline var ENTER: KeyCode = new KeyCode(13);
  public static inline var SPACE: KeyCode = new KeyCode(32);
  public static inline var UP: KeyCode = new KeyCode(38);
  public static inline var DOWN: KeyCode = new KeyCode(40);
  public static inline var LEFT: KeyCode = new KeyCode(37);
  public static inline var RIGHT: KeyCode = new KeyCode(39);
  public static inline var PGUP: KeyCode = new KeyCode(33);
  public static inline var PGDN: KeyCode = new KeyCode(34);
  public static inline var HOME: KeyCode = new KeyCode(36);
  public static inline var END: KeyCode = new KeyCode(35);
  public static inline var TAB: KeyCode = new KeyCode(9);
  public static inline var CONTROL: KeyCode = new KeyCode(17);
  public static inline var SHIFT: KeyCode = new KeyCode(16);
  public static inline var ESCAPE: KeyCode = new KeyCode(27);
  public static inline var INSERT: KeyCode = new KeyCode(45);
  public static inline var DELETEKEY: KeyCode = new KeyCode(46);
  public static inline var BACKSPACE: KeyCode = new KeyCode(8);
  public static inline var CAPSLOCK: KeyCode = new KeyCode(20);

  public static inline var NUMBER_0: KeyCode = new KeyCode(0x30); // 48
  public static inline var NUMBER_1: KeyCode = new KeyCode(0x31);
  public static inline var NUMBER_2: KeyCode = new KeyCode(0x32);
  public static inline var NUMBER_3: KeyCode = new KeyCode(0x33);
  public static inline var NUMBER_4: KeyCode = new KeyCode(0x34);
  public static inline var NUMBER_5: KeyCode = new KeyCode(0x35);
  public static inline var NUMBER_6: KeyCode = new KeyCode(0x36);
  public static inline var NUMBER_7: KeyCode = new KeyCode(0x37);
  public static inline var NUMBER_8: KeyCode = new KeyCode(0x38);
  public static inline var NUMBER_9: KeyCode = new KeyCode(0x39); // 57

  public static inline var A: KeyCode = new KeyCode(0x41); // 65
  public static inline var B: KeyCode = new KeyCode(0x42);
  public static inline var C: KeyCode = new KeyCode(0x43);
  public static inline var D: KeyCode = new KeyCode(0x44);
  public static inline var E: KeyCode = new KeyCode(0x45);
  public static inline var F: KeyCode = new KeyCode(0x46);
  public static inline var G: KeyCode = new KeyCode(0x47);
  public static inline var H: KeyCode = new KeyCode(0x48);
  public static inline var I: KeyCode = new KeyCode(0x49);
  public static inline var J: KeyCode = new KeyCode(0x4a);
  public static inline var K: KeyCode = new KeyCode(0x4b);
  public static inline var L: KeyCode = new KeyCode(0x4c);
  public static inline var M: KeyCode = new KeyCode(0x4d);
  public static inline var N: KeyCode = new KeyCode(0x4e);
  public static inline var O: KeyCode = new KeyCode(0x4f);
  public static inline var P: KeyCode = new KeyCode(0x50);
  public static inline var Q: KeyCode = new KeyCode(0x51);
  public static inline var R: KeyCode = new KeyCode(0x52);
  public static inline var S: KeyCode = new KeyCode(0x53);
  public static inline var T: KeyCode = new KeyCode(0x54);
  public static inline var U: KeyCode = new KeyCode(0x55);
  public static inline var V: KeyCode = new KeyCode(0x56);
  public static inline var W: KeyCode = new KeyCode(0x57);
  public static inline var X: KeyCode = new KeyCode(0x58);
  public static inline var Y: KeyCode = new KeyCode(0x59);
  public static inline var Z: KeyCode = new KeyCode(0x5a); // 90

  public static inline var NUMPAD_0: KeyCode = new KeyCode(0x60); // 96
  public static inline var NUMPAD_1: KeyCode = new KeyCode(0x61);
  public static inline var NUMPAD_2: KeyCode = new KeyCode(0x62);
  public static inline var NUMPAD_3: KeyCode = new KeyCode(0x63);
  public static inline var NUMPAD_4: KeyCode = new KeyCode(0x64);
  public static inline var NUMPAD_5: KeyCode = new KeyCode(0x65);
  public static inline var NUMPAD_6: KeyCode = new KeyCode(0x66);
  public static inline var NUMPAD_7: KeyCode = new KeyCode(0x67);
  public static inline var NUMPAD_8: KeyCode = new KeyCode(0x68);
  public static inline var NUMPAD_9: KeyCode = new KeyCode(0x69); // 105
  public static inline var NUMPAD_MULTIPLY: KeyCode = new KeyCode(0x6a); // 106
  public static inline var NUMPAD_ADD: KeyCode = new KeyCode(0x6b);
  public static inline var NUMPAD_ENTER: KeyCode = new KeyCode(0x6c);
  public static inline var NUMPAD_SUBTRACT: KeyCode = new KeyCode(0x6d);
  public static inline var NUMPAD_DECIMAL: KeyCode = new KeyCode(0x6e);
  public static inline var NUMPAD_DIVIDE: KeyCode = new KeyCode(0x6f); // 111

  public inline function new(code: Int): Void {
    this = code;
  }

  public inline function toInt(): Int {
    return this;
  }
}
