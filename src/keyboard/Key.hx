package keyboard;

import etwin.flash.Key in FlashKey;
import keyboard.KeyCode;

/**
 * Adapter around the native Flash `Key` class.
 */
class Key {
  /**
	 * Returns the ASCII code of the last keyboard pressed or released.
   */
  public static inline function getAscii(): Int {
    return FlashKey.getAscii();
  }

  /**
	 * Returns the keyboard code value of the last keyboard pressed.
   */
  public static inline function getCode(): KeyCode {
    return new KeyCode(FlashKey.getCode());
  }

  /**
	 * Returns a Boolean value indicating, depending on security restrictions,
	 * whether the last keyboard pressed may be accessed by other SWF files.
   */
  public static inline function isAccessible(): Bool {
    return FlashKey.isAccessible();
  }

  /**
	 * Returns `true` if the keyboard specified in _code_ is pressed; `false` otherwise.
   */
  public static inline function isDown(code: KeyCode): Bool {
    return FlashKey.isDown(code.toInt());
  }

  /**
	 * Returns `true` if the Caps Lock or Num Lock keyboard is activated (toggled to
	 * an active state); `false` otherwise.
   */
  public static inline function isToggled(code: KeyCode): Bool {
    return FlashKey.isToggled(code.toInt());
  }

  /**
	 * Registers an object to receive `onKeyDown` and `onKeyUp` notification.
   */
  public static inline function addListener(listener: KeyListener): Void {
    return FlashKey.addListener(listener);
  }

  /**
	 * Removes an object previously registered with `Key.addListener()`.
   */
  public static inline function removeListener(listener: KeyListener): Bool {
    return FlashKey.removeListener(listener);
  }
}
