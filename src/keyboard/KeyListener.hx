package keyboard;

typedef KeyListener = {
  function onKeyDown(): Void;

  function onKeyUp(): Void;
}
